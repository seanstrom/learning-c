#ifndef List_h
#define List_h

#include<stdlib.h>

struct ListNode;

struct ListNode {
  struct ListNode* prev;
  struct ListNode* next;
  void* value;
};

typedef struct ListNode ListNode;

struct List {
  int count;
  ListNode* first;
  ListNode* last;
};

ListNode* ListNode_create(int data, ListNode *prev, ListNode *next) {
  ListNode* node = NULL;
  node = malloc(sizeof(ListNode));
  node->data = data;
  node->prev = prev;
  node->next = next;
  return node;
}

void ListNode_destroy(ListNode* node)
{
  if (node == NULL) return;
  free(node);
}

void ListNodes_destroy(ListNode* node) {
  if (node != NULL) {
    ListNodes_destroy(node);
    free(node);
  }
}

typedef struct List {
  int head;
  struct List* tail;
  struct ListNode* node;
} List;

List* List_create(ListNode* node)
{
  List* list = NULL;
  list = malloc(sizeof(List));

  if (node != NULL) {
    list->head = node->data;
    list->node = node;

    if (node->next != NULL) {
      list->tail = List_create(node->next);
    } else {
      list->tail = NULL;
    }
  }

  return list;
}

void List_destroy(List* list)
{
  if (list != NULL) {
    if (list->tail != NULL) {
      List_destroy(list->tail);
    } else {
      ListNode_destroy(list->node);
      free(list);
    }
  }
}

List* List_from_Array(int nums[], int len)
{
  ListNode* current_node = NULL;
  List* list = List_create(current_node);
  return list;
}

ListNode* ListNodes_from_Array(int nums[], int len)
{
  int i = len;
  ListNode* current_node = NULL;

  for (i = len; i >= 0; i--) {
    int data = nums[i];
    ListNode* node = ListNode_create(data, NULL, current_node);
    current_node = node;
  }

  return current_node;
}

void main(int argc, char *argv[])
{
  int nums[] = {1, 2, 3};
  int nums_len = sizeof(nums) / sizeof(nums[0]);

  /* List *list = List_from_Array(nums, nums_len); */
  /* printf("%d\n", list->head); */
  /* List_destroy(list); */

  ListNode* list_node = ListNodes_from_Array(nums, nums_len);
  printf("%d\n", list_node->data);
  ListNodes_destroy(list_node);
}
